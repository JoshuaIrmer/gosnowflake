.PHONY: all lint lint.struct-aligement fmt test test.coverage test.coverage.html clean
all: fmt lint test

lint:
	golangci-lint run

lint.struct-aligement:
	go vet -vettool=$(shell which fieldalignment) ./...

fmt:
	go fmt ./...

test:
	go test ./...

test.coverage:
	go test ./... -v -coverprofile=coverage.out
	rm coverage.out

test.coverage.html:
	go test ./... -coverprofile=coverage.out
	go tool cover -html=coverage.out
	rm coverage.out

clean:
	go clean ./...
