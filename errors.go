package gosnowflake

import "errors"

var (
	// ErrInvalidInstanceID is an error thrown, when the instance ID is invalid.
	ErrInvalidInstanceID = errors.New("invalid instance id")
	// ErrInvalidSequence is an error thrown, when the sequence is invalid.
	ErrInvalidSequence = errors.New("invalid sequence number")
	// ErrInvalidTimestamp is an error thrown, when the timestamp is invalid.
	ErrInvalidTimestamp = errors.New("invalid timestamp")
)
