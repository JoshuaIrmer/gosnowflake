package gosnowflake

import (
	"time"
)

const (
	timeOffset     uint64 = 22
	instanceOffset uint64 = 12
	instanceMask   uint64 = 0x3FF
	sequenceMask   uint64 = 0xFFF
)

type SnowflakeDataInterface interface {
	GetTimestamp() time.Time
	SetTimestamp(time.Time) error
	GetInstance() uint16
	GetSequence() uint16
	SetSequence(uint16) error
}

type SnowflakeData struct {
	Timestamp time.Time // 41 bits
	Instance  uint16    // 10 bits
	Sequence  uint16    // 12 bits
}

func (sfd SnowflakeData) GetTimestamp() time.Time {
	return sfd.Timestamp
}

func (sfd *SnowflakeData) SetTimestamp(timestamp time.Time) error {
	if timestamp.Unix() > sfd.Timestamp.Unix() { // new timestamp cannot be older than last timestamp
		return ErrInvalidTimestamp
	}

	sfd.Timestamp = timestamp

	return nil
}

func (sfd SnowflakeData) GetInstance() uint16 {
	return sfd.Instance
}

func (sfd SnowflakeData) GetSequence() uint16 {
	return sfd.Sequence
}

func (sfd *SnowflakeData) SetSequence(sequence uint16) error {
	if sequence < 1 || sequence > 4095 {
		return ErrInvalidSequence
	}

	sfd.Sequence = sequence

	return nil
}
