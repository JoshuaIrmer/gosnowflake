package gosnowflake_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestGosnowflake(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Gosnowflake Suite")
}
