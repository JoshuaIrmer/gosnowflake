package gosnowflake

import (
	"time"
)

type SnowflakeInterface interface {
	GetData() (SnowflakeDataInterface, error)
}

type Snowflake uint64

func (sf Snowflake) GetData() (SnowflakeDataInterface, error) {
	timeBits := uint64(sf) >> timeOffset
	instanceBits := uint64(sf) >> instanceOffset & instanceMask
	sequenceBits := uint64(sf) & sequenceMask

	d := SnowflakeData{
		Timestamp: time.Unix(int64(timeBits), 0),
		Instance:  uint16(instanceBits),
		Sequence:  uint16(sequenceBits),
	}

	return &d, nil
}
