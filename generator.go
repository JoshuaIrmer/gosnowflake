package gosnowflake

import (
	"fmt"
	"time"
)

type GeneratorInterface interface {
	Generate() (SnowflakeInterface, error)
}

type Generator struct {
	data SnowflakeDataInterface
}

func NewGenerator(instanceID uint16) (GeneratorInterface, error) {
	if instanceID < 1 || instanceID > 1023 {
		return nil, ErrInvalidInstanceID
	}

	return Generator{
		data: &SnowflakeData{
			Timestamp: time.Now(),
			Instance:  instanceID,
			Sequence:  0,
		},
	}, nil
}

func (sfg Generator) Generate() (SnowflakeInterface, error) {
	now := time.Now()

	if sfg.data.GetTimestamp().Unix() == now.Unix() {
		err := sfg.data.SetSequence(sfg.data.GetSequence() + 1)
		if err != nil {
			return nil, fmt.Errorf("error increment sequence: %w", err)
		}
	} else {
		err := sfg.data.SetTimestamp(now)
		if err != nil {
			return nil, fmt.Errorf("error setting timestamp: %w", err)
		}

		err = sfg.data.SetSequence(1)
		if err != nil {
			return nil, fmt.Errorf("error resetting sequence: %w", err)
		}
	}

	timeBits := uint64(sfg.data.GetTimestamp().Unix()) << timeOffset
	instanceBits := uint64(sfg.data.GetInstance()) << instanceOffset
	sequenceBits := uint64(sfg.data.GetSequence())

	newSnowflake := timeBits | instanceBits | sequenceBits

	return Snowflake(newSnowflake), nil
}
