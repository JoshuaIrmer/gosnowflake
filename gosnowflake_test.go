package gosnowflake_test

import (
	"os"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"gitlab.com/JoshuaIrmer/gosnowflake"
)

var _ = Describe("Gosnowflake", func() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: "2006-01-02T15:04:05"})

	Context("with valid instance id", func() {
		It("can create Snowflake generator", func() {
			// Arrange
			instanceID := uint16(1)

			// Act
			g, err := gosnowflake.NewGenerator(instanceID)

			// Assert
			Ω(err).ShouldNot(HaveOccurred(), "generator error")
			Ω(g).ShouldNot(BeNil(), "generator exists")
		})

		It("can generate multiple Snowflakes", func() {
			// Arrange
			instanceID := uint16(5)
			g, _ := gosnowflake.NewGenerator(instanceID)

			// Act
			sf1, err1 := g.Generate()
			sf2, err2 := g.Generate()
			// sf3, err3 := g.Generate()

			// Assert
			Ω(err1).ShouldNot(HaveOccurred(), "snowflake 1 error")
			Ω(err2).ShouldNot(HaveOccurred(), "snowflake 2 error")
			// Ω(err3).ShouldNot(HaveOccurred(), "snowflake 3 error")

			sf1d, _ := sf1.GetData()
			sf2d, _ := sf2.GetData()
			// sf3d, _ := sf3.GetData()

			Ω(sf1d.GetInstance()).Should(Equal(uint16(5)), "snowflake 1 instance")
			Ω(sf1d.GetSequence()).Should(Equal(uint16(1)), "snowflake 1 sequence")

			Ω(sf2d.GetInstance()).Should(Equal(uint16(5)), "snowflake 2 instance")
			Ω(sf2d.GetSequence()).Should(Equal(uint16(2)), "snowflake 2 sequence")

			// Ω(sf3d.GetInstance()).Should(Equal(uint16(5)), "snowflake 3 instance")
			// Ω(sf3d.GetSequence()).Should(Equal(uint16(3)), "snowflake 3 sequence")
		})
	})

	Context("with invalid instance id", func() {
		It("should generate error", func() {
			// Arrange
			instanceID := uint16(0)

			// Act
			g, err := gosnowflake.NewGenerator(instanceID)

			// Assert
			Ω(g).Should(BeNil(), "generator does not exists")
			Ω(err).Should(HaveOccurred(), "generator error")
		})
	})

	Context("Parser with valid input", func() {
		It("can decode Snowflake", func() {
			// Arrange
			sf := gosnowflake.Snowflake(6802656660033537)
			time, _ := time.Parse(time.RFC3339, "2021-05-24T20:08:55+02:00")

			// Act
			d, err := sf.GetData()

			// Assert
			Ω(err).ShouldNot(HaveOccurred(), "data error")
			Ω(d.GetTimestamp().Unix()).Should(Equal(time.Unix()), "timestamp in unix time")
			Ω(d.GetInstance()).Should(Equal(uint16(1)), "instance id")
			Ω(d.GetSequence()).Should(Equal(uint16(1)), "sequence")
		})
	})
})
