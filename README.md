# GoSnowflake

[![pipeline status](https://gitlab.com/JoshuaIrmer/gosnowflake/badges/master/pipeline.svg)](https://gitlab.com/JoshuaIrmer/gosnowflake/-/commits/master)
[![coverage report](https://gitlab.com/JoshuaIrmer/gosnowflake/badges/master/coverage.svg)](https://gitlab.com/JoshuaIrmer/gosnowflake/-/commits/master)

My implementation of a snowflake datatype. \
This is only public because I use it in my other projects. If you want to use it to, feel free.

## Features

- Unix timestamp support till 71654-04-10
- 1 to 1023 instances
- 1 to 4095 snowflakes per second
- instance id freely configuratable

## Bit Layout

- 41 bits for the second precission timestamp. (Support till 71654-04-10)
- 10 bits for the instace of the generator (1-1023)
- 12 bits for the sequence (1-4095)

### Offsets

Used offsets:

```go
timeOffset     uint64 = 22
instanceOffset uint64 = 12
```

### Masks

Used bitmaskes:

```go
instanceMask   uint64 = 0x3FF
sequenceMask   uint64 = 0xFFF
```

## Examples

### Generate new snowflake

```go
generator, err := gosnowflake.NewGenerator(yourInstanceID)
if err != nil {
  panic(err) // invalid instance id
}

snowflake := generator.Generate() // your snowflake
```

### Get data from snowflake

```go
snowflake := gosnowflake.Snowflake(6802656660033537) // snowflake from decimal
data := snowflake.GetData

timestamp := data.GetTimestamp() // 2021-05-24T20:08:55+02:00
instance := data.GetInstance() // 1
sequence := data.GetSequence() // 1
```
