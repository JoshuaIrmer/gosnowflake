module gitlab.com/JoshuaIrmer/gosnowflake

go 1.16

require (
	github.com/onsi/ginkgo v1.16.4
	github.com/onsi/gomega v1.16.0
	github.com/rs/zerolog v1.23.0
)
